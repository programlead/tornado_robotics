# LEGO type:standard slot:19 autostart

import sys, util

def import_from_slot(slot):
    if '/projects' not in sys.path:
        sys.path.append('projects')
        slot_path = util.storage.get_path(slot)
    return __import__(slot_path.split('/')[-1])

for slot in [1, 2, 3, 4, 5]:
    print('Run: ', slot)
    try: 
        import_from_slot(slot)
    except SystemExit:
        pass

raise SystemExit
