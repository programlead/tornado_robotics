# LEGO type:standard slot:3 autostart

'''
2022/10/15 faster, asynchronized motors, gyro straight use distance
2022/10/23 use slow acceleration gyro straight
'''

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from spike.operator import greater_than, less_than, equal_to
from math import *
import hub as hub_module
import sys

# initialization
hub = PrimeHub()
left = Motor('E')
right = Motor('F')
arm_left = Motor('C')
arm_left_nonblock = hub_module.port.C.motor
arm_right = Motor('D')
m = MotorPair('E', 'F')
#t = Timer()

# define function(s)
def turn_to(target, power): # gyro turn
    yaw = hub.motion_sensor.get_yaw_angle()
    if yaw < target:
        m.start_tank_at_power(power,-power)
        wait_until(hub.motion_sensor.get_yaw_angle, greater_than, target)
        m.stop()
    else:
        m.start_tank_at_power(-power,power)
        wait_until(hub.motion_sensor.get_yaw_angle, less_than, target)
        m.stop()

def get_current_power(acceleration_dist, power): # for acceleration
    rotations = acceleration_dist/(5.6 * 3.14159)
    degrees = rotations * 360
    current_power = (power - 30)/degrees * right.get_degrees_counted() + 30
    if current_power > power:
        current_power = power
    return current_power


def gyro_straight(degree, angle, kp, power): # gyro straight
    right.set_degrees_counted(0)
    while (right.get_degrees_counted() < degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        current_power = round(get_current_power(10, power))
        m.start_tank_at_power(current_power - correction, current_power + correction)
    m.stop()

def gyro_straight_dist(centimeters, angle, kp, power):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power)

# wait for arrow, reduce time delay before run
def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('3') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)
    myhub.status_light.on('blue')


###########################
# start calling functions #
###########################

#raise SystemExit
start_angle = 0
wait_any_arrow_press()
hub.motion_sensor.reset_yaw_angle()
t = Timer()

# get to missions
arm_left_nonblock.run_for_time(750, max_power=30)
gyro_straight_dist(67, 0 - start_angle, 1, 80)
#print('Gyro time: {} seconds'.format(t.now()))
turn_to(42, 30)

# start missions
m.move(20, 'cm', 0, 40)
wait_for_seconds(0.5)
for i in range(3):
    arm_left_nonblock.run_for_degrees(-70, -50)
    m.move(-6, 'cm', 0, 40)
    wait_for_seconds(0.1)
    arm_left_nonblock.run_for_degrees(80, 50)
    m.move(20, 'cm', 0, 40)
    wait_for_seconds(0.5)
    #arm_left.run_for_degrees(-45, 50)
    #arm_left.run_for_degrees(50, 50)
m.move(-10, 'cm', 0, 40)
turn_to(65 - start_angle, 40)
m.move(-14, 'cm')
turn_to(-40 - start_angle, 30)
m.move(-80, 'cm')
m.stop()
print('Total Time = {} seconds'.format(t.now()))
arm_right.run_to_position(48)
raise SystemExit
