'''
Trying to connect to serial port
to see if there is a Spike Prime Hub

need to install serial module in python:
pip install serial
'''

import serial
from serial.tools import list_ports
import time

def wait_for_prompt(ser):
  buf = b''
  start_time = time.time()
  elapsed = 0
  while elapsed < 1:
    c = ser.in_waiting
    ser.timeout = 1 - elapsed
    x = ser.read(c if c else 1)
    buf = (buf + x)[-5:]
    if buf == b'\n>>> ':
       return
    elapsed = time.time() - start_time
  raise ConnectionError('failed to get to the command prompt (last characters: %s)' % buf)

def write_command(ser, cmd):
  ser.write(cmd + b'\r\n')

def get_hub_name(ser):
  write_command(ser, b"f = open('etc/hostname', 'rb')")
  result = ser.read(192)
  write_command(ser, b"f.read(192)")
  result = ser.read(192);
  write_command(ser, b"f.close()")
  if b"f.read(192)" in result:
    result = str(result[14:]).split('\'')
    if len(result) >= 3:
      return result[1]
  print('Failed to get Hub name')

def main(num_of_hubs=1):
  port_names = sorted([p.name for p in serial.tools.list_ports.comports()])
  #port_names = ['COM4']
  found = 0
  for port_name in port_names:
      try:
          print('trying {}'.format(port_name))
          ser = serial.Serial(port_name, timeout=1, write_timeout=1.1)         
          ser.write(b'\x03')
          wait_for_prompt(ser)

          # get hub name
          print('****** {} : {} ******'.format(port_name, get_hub_name(ser)))
          ser.close()
          found += 1
          if found >= num_of_hubs:
            break
      except Exception as e:
          print('Failed to connect to {}: {}'.format(port_name, e))

main(1)
