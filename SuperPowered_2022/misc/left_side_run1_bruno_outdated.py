from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *
from spike.operator import less_than, greater_than, equal_to

timer = Timer()
hub = PrimeHub()
hub.motion_sensor.reset_yaw_angle()
legs = MotorPair('E', 'F')
legs.set_default_speed(50)
motorD = Motor ('D')
motorC = Motor ('C')
colorA = ColorSensor ('A')
colorB = ColorSensor('B')

def turn_to(target):
    yaw = hub.motion_sensor.get_yaw_angle()
    if yaw < target:
        legs.start_tank_at_power(30,-30)
        wait_until(hub.motion_sensor.get_yaw_angle, greater_than, target)
        legs.stop()
    else:
        legs.start_tank_at_power(-30,30)
        wait_until(hub.motion_sensor.get_yaw_angle, less_than, target)
        legs.stop()

def turn_left_to(target):
    legs.start_tank_at_power(0, 50)
    if (hub.motion_sensor.get_yaw_angle() >= 0)  :
        wait_until(hub.motion_sensor.get_yaw_angle, less_than, target)
    else :
        if (target < 0) :
            wait_until(hub.motion_sensor.get_yaw_angle, less_than, target)
        else :
             legs.stop()   
    legs.stop()

def turn_left_back_to(target):
    legs.start_tank_at_power(0, -50)
    if (hub.motion_sensor.get_yaw_angle() < 0):
        wait_until(hub.motion_sensor.get_yaw_angle, greater_than, target)
    else :
        if (target >= 0) :
            wait_until(hub.motion_sensor.get_yaw_angle, greater_than , target)
        else :
            legs.stop()
    legs.stop()

def turn_right_to(target):
    legs.start_tank_at_power(50, 0)
    if (hub.motion_sensor.get_yaw_angle() < 0):
        wait_until(hub.motion_sensor.get_yaw_angle, greater_than, target)
    else :
        if (target >= 0) :
            wait_until(hub.motion_sensor.get_yaw_angle, greater_than , target)
        else :
            legs.stop()
    legs.stop()

def turn_right_back_to(target):
    legs.start_tank_at_power(-30, 0)
    if (hub.motion_sensor.get_yaw_angle() >= 0):
        wait_until(hub.motion_sensor.get_yaw_angle, less_than, target)
    else :
        if (target < 0) :
            wait_until(hub.motion_sensor.get_yaw_angle, less_than , target)
        else :
            legs.stop()
    legs.stop()

def move_forward(degree,angle,speed):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    kp = 1
    while (motor.get_degrees_counted() < degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(speed - correction, speed + correction)
        print (motor.get_degrees_counted())
    legs.stop()

def move_backward(degree,angle):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = -50
    kp = 1
    while (motor.get_degrees_counted() > degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
        print (motor.get_degrees_counted())
    legs.stop()

def stop_at_line_right(angle,power):
    kp = 1
    print(colorB.get_reflected_light())
    while colorB.get_reflected_light() > 25:
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
        print(colorB.get_reflected_light())
    legs.stop()

def stop_at_line_left(angle,power):
    kp = 1
    print(colorA.get_reflected_light())
    while colorA.get_reflected_light() > 25:
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
        print(colorA.get_reflected_light())
    legs.stop()

def stop_at_line_right_tank(power):
    legs.start_tank_at_power(power, power)
    wait_until(colorB.get_reflected_light, greater_than, 90)
    wait_until(colorB.get_reflected_light, less_than, 30)
    wait_until(colorB.get_reflected_light, greater_than, 90)
    legs.stop()

def stop_at_line_left_tank(power):
    print(colorA.get_reflected_light())
    while colorA.get_reflected_light() > 25:
        legs.start_tank_at_power(power, power)
        print(colorA.get_reflected_light())
    legs.stop()

def move_forward_for( seconds , angle):
    power = 50
    timer.reset()
    while (timer.now() < seconds):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) 
        legs.start_tank_at_power(power - correction, power + correction)
    legs.stop()

def follow_outside_of_line(degrees):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = 30
    kp = 1
    while motor.get_degrees_counted() < degrees:
        correction = (colorB.get_reflected_light() - 60) * kp
        legs.start_tank_at_power(power - correction, power + correction)
    legs.stop()

def follow_inside_of_line(degrees):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = 30
    while motor.get_degrees_counted() < degrees:
        correction = (colorB.get_reflected_light() - 60) 
        legs.start_tank_at_power(power + correction, power - correction)
    legs.stop()

def lift_rack_and_pinion_for(seconds):
    motor = Motor('C')
    motor.set_default_speed(-100)
    motor.run_for_seconds(seconds)

def lower_rack_and_pinion_for(seconds):
    motor = Motor('C')
    motor.set_default_speed(100)
    motor.run_for_seconds(seconds)

def home_to_M11():
    stop_at_line_right(0,40)
    move_forward(170,0,50)
    turn_to(135)

def M11():
    move_forward(750,135,50)
    turn_to(50)
    move_backward(-300,50)

def M11_to_M10():
    turn_to(78)
    stop_at_line_right(78,35)
    move_backward(-100,178)
    turn_to(178)

def M10():
    lift_rack_and_pinion_for(1)
    stop_at_line_right_tank(30)
    lower_rack_and_pinion_for(1)
    legs.move_tank(15,'cm',-30,-30)

def M10_to_M5():
    turn_right_back_to(90)
    stop_at_line_right(90,30)
    turn_left_to(0)
    lift_rack_and_pinion_for(0.5)
    stop_at_line_left(0,30)

def M5():
    move_forward(200,0,30)
    lower_rack_and_pinion_for(0.4)
    move_backward(-30,0)
    lift_rack_and_pinion_for(0.4)
    stop_at_line(0,-30)

def M5_to_M2():
    lower_rack_and_pinion_for(0.8)
    turn_left_to(-90)
    stop_at_line_left(-90,30)
    move_forward(50,-90,40)

def M2():
    count = 0
    while (count < 3):
        lift_rack_and_pinion_for(0.7)
        lower_rack_and_pinion_for(0.7)
        count = count + 1


def M3():
    #move_forward_stopping_at_line(0)
    motor = Motor('C')
    motor.run_for_rotations(2.5, 50)
    motor.run_for_rotations(2.5, -50)

def M2_to_M3():
    turn_to(5)

def M3_to_home():
    move_backward(-540, -90)
    move_backward(-720, -65)

home_to_M11()
M11()
M11_to_M10()
M10()
M10_to_M5()
M5()
#M5_to_M2()
#M2()
