# LEGO type:standard slot:6 autostart

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from spike.operator import greater_than, less_than, equal_to
from math import *
import hub as hub_module
import sys

# initialization
hub = PrimeHub()
left_eye = ColorSensor('A')
right_eye = ColorSensor('B')
left = Motor('E')
right = Motor('F')
arm_left = Motor('C')
arm_left_nonblock = hub_module.port.C.motor
arm_right = Motor('D')
arm_right_nonblock = hub_module.port.D.motor
m = MotorPair('E', 'F')

class LineFollower:
    def __init__(self, t_rli, power, kp, ki=0, kd=0):
        self.I = 0
        self.last_err = 0

        self.t_rli = t_rli
        self.power = power
        self.kp = kp
        self.ki = ki
        self.kd = kd

    def follow_line_one_step(self):
        err = left_eye.get_reflected_light() - self.t_rli
        D = err - self.last_err
        cor = (err * self.kp + self.I * self.ki + D * self.kd) * 1.5
        self.I = self.I * 0.99 + err
        self.last_err = err
        m.start_tank_at_power(round(self.power - cor), round(self.power + cor))

    def follow_line_forever(self):
        while True:
            self.follow_line_one_step()

    def follow_line_distance(self, centimeters):
        right.set_degrees_counted(0)
        rotations = centimeters/(5.6 * 3.14159)
        degrees = rotations * 360
        while (right.get_degrees_counted() < degrees):
            self.follow_line_one_step()

line_follower = LineFollower(50, 40, -0.2, -0.001, -2)
line_follower.follow_line_distance(52)
