# LEGO type:standard slot:2 autostart

'''
10/30/2022 initial commit
'''

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from spike.operator import greater_than, less_than, equal_to
from math import *
import hub as hub_module
import sys

# initialization
hub = PrimeHub()
left = Motor('E')
right = Motor('F')
arm_left = Motor('C')
arm_left_nonblock = hub_module.port.C.motor
arm_right = Motor('D')
m = MotorPair('E', 'F')
hub.motion_sensor.reset_yaw_angle()
t = Timer()

# define functions
class GlobalYaw:
    def __init__(self):
        self.last_yaw = hub.motion_sensor.get_yaw_angle()
        self.rotations = 0

    def get_global_yaw(self):
        current_yaw = hub.motion_sensor.get_yaw_angle()
        if self.last_yaw - current_yaw >= 180:
            self.rotations += 1
        elif self.last_yaw - current_yaw <= -180:
            self.rotations -= 1
        global_yaw = current_yaw + self.rotations * 360
        self.last_yaw = current_yaw
        return global_yaw

global_yaw = GlobalYaw()

def gyro_straight(degree, angle, kp, power): # gyro straight
    right.set_degrees_counted(0)
    while (right.get_degrees_counted() < degree):
        #correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        correction = (global_yaw.get_global_yaw() - angle) * kp
        m.start_tank_at_power(power - correction, power + correction)
    m.stop()

def gyro_straight_dist(centimeters, angle, kp, power):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power)

# start
gyro_straight_dist(45, 0, 1, 50)
left.run_for_degrees(10)
right.run_for_degrees(10)
left.run_for_degrees(10)
right.run_for_degrees(10)
arm_right.run_for_rotations(-5, 100)
arm_right.run_for_rotations(5, 100)
m.move(-40, 'cm', 0, 100)


# total time
print('Total time = {} seconds'.format(t.now()))


