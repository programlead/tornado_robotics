# LEGO type:standard slot:19 autostart

'''
10/30/2022 initial commit
10/31/2022 to smart grid
11/05/2022 full run
'''

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from spike.operator import greater_than, less_than, equal_to
from math import *
import hub as hub_module
import sys

# initialization
hub = PrimeHub()
left_eye = ColorSensor('A')
right_eye = ColorSensor('B')
left = Motor('E')
right = Motor('F')
arm_left = Motor('C')
arm_left_nonblock = hub_module.port.C.motor
arm_right = Motor('D')
arm_right_nonblock = hub_module.port.D.motor
m = MotorPair('E', 'F')

m.move(80, 'cm', steering=10, speed=80)
