# LEGO type:standard slot:5 autostart

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *
from spike.operator import greater_than_or_equal_to

hub = PrimeHub()

a = ColorSensor("A")
b = ColorSensor("B")
c = Motor("C")
d = Motor("D")
e = Motor("E")
f = Motor("F")
right = Motor('F')
m = MotorPair("E","F")
t = Timer()
hub.motion_sensor.reset_yaw_angle()

def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('5') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)
    myhub.status_light.on('blue')

def get_current_power(acceleration_dist, power): # for acceleration
    rotations = acceleration_dist/(5.6 * 3.14159)
    degrees = rotations * 360
    current_power = (power - 30)/degrees * right.get_degrees_counted() + 30
    if current_power > power:
        current_power = power
    return current_power

def gyro_straight(degree, angle, kp, power, stop=True): # gyro straight
    right.set_degrees_counted(0)
    while (fabs(right.get_degrees_counted()) < degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        current_power = round(get_current_power(10, power))
        m.start_tank_at_power(current_power - correction, current_power + correction)
    if stop:
        m.stop()

def gyro_straight_dist(centimeters, angle, kp, power, stop=True):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power, stop=stop)

# do the rechargeable battery
#gyro_straight_dist(17 * 2.54, 0, 1, 60)
#gyro_straight_dist(-18 * 2.54, 0, 1, 60)
wait_any_arrow_press()
hub.motion_sensor.reset_yaw_angle()
gyro_straight_dist(14 * 2.54, 0, 1, -60)
gyro_straight_dist(15 * 2.54, 0, 1, 60)

'''
m.start_tank_at_power(0, 20)
while hub.motion_sensor.get_yaw_angle() > -40: pass
m.stop()
'''

# carry the dinosaur
print(hub.motion_sensor.get_yaw_angle())
m.start_tank_at_power(-30, 30)
while hub.motion_sensor.get_yaw_angle() > -32: pass
m.stop()
print(hub.motion_sensor.get_yaw_angle())
#wait_any_arrow_press()
hub.motion_sensor.reset_yaw_angle()
gyro_straight_dist(80, 0, 1, -90, stop=False)
gyro_straight_dist(105, -30, 1, -90)

raise SystemExit
