# LEGO type:standard slot:5 autostart

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *
from spike.operator import greater_than_or_equal_to

# initialization
hub = PrimeHub()
left = Motor('E')
right = Motor('F')
m = MotorPair('E', 'F')
t = Timer()

# wait for any arrow to be pressed
def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('5') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)

# define the gyro straight with acceleration
def get_current_power(acceleration_dist, power): # for acceleration
    rotations = acceleration_dist/(5.6 * 3.14159)
    degrees = rotations * 360
    current_power = (power - 30)/degrees * right.get_degrees_counted() + 30
    if current_power > power:
        current_power = power
    return current_power

def gyro_straight(degree, angle, kp, power): # gyro straight
    right.set_degrees_counted(0)
    while (right.get_degrees_counted() < degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        current_power = round(get_current_power(15, power))
        m.start_tank_at_power(current_power - correction, current_power + correction)
    m.stop()

def gyro_straight_dist(centimeters, angle, kp, power):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power)

# carry the dinosaur
wait_any_arrow_press()
hub.motion_sensor.reset_yaw_angle()
gyro_straight_dist(250, 0, 1, 90)

# print total time
print('Total time = {} seconds '.format(t.now()))
