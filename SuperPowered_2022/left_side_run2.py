# LEGO type:standard slot:2 autostart

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *
from spike.operator import less_than, greater_than
import hub

timer = Timer()
robot = PrimeHub()
legs = MotorPair('E', 'F')
legs.set_default_speed(50)
motorD = Motor ('D')
motorC = Motor ('C')
colorA = ColorSensor ('A')
colorB = ColorSensor ('B')

def turn_to(target):
    yaw = robot.motion_sensor.get_yaw_angle()
    if yaw < fabs(target):
        #print: fabs(target)
        legs.start_tank_at_power(40,-40)
        wait_until(robot.motion_sensor.get_yaw_angle, greater_than, target)
        legs.stop()
    else:
        legs.start_tank_at_power(-40,40)
        wait_until(robot.motion_sensor.get_yaw_angle, less_than, target)
        legs.stop()


def move_forward(degree,angle,speed,stop=True):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    kp = 1
    while (motor.get_degrees_counted() < degree):
        correction = (robot.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(speed - correction, speed + correction)
        #print (motor.get_degrees_counted())

    if stop:
        legs.stop()

def move_backward(degree,angle):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = -50
    kp = 1
    while (motor.get_degrees_counted() > degree):
        correction = (robot.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
        #print (motor.get_degrees_counted())
    legs.stop()

def stop_at_line(angle, power):
    kp = 1
    #print(colorB.get_reflected_light())
    while colorB.get_reflected_light() > 25:
        correction = (robot.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
        #print(colorB.get_reflected_light())
    legs.stop()

def move_forward_for( seconds , angle):
    power = 50
    kp = 1
    timer.reset()
    while (timer.now() < seconds):
        correction = (robot.motion_sensor.get_yaw_angle() - angle) * kp
        legs.start_tank_at_power(power - correction, power + correction)
    legs.stop()

def follow_outside_of_line(degrees):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = 30
    kp = 1
    while motor.get_degrees_counted() < degrees:
        correction = (color.get_reflected_light() - 60) * kp # FIXME: color is not defined
        legs.start_tank_at_power(power - correction, power + correction)
    legs.stop()

def follow_inside_of_line(degrees):
    motor = Motor('F')
    motor.set_degrees_counted(0)
    power = 30
    kp = 1
    while motor.get_degrees_counted() < degrees:
        correction = (color.get_reflected_light() - 60) * kp # FIXME: color is not defined
        legs.start_tank_at_power(power + correction, power - correction)
    legs.stop()

def lift_rack_and_pinion_for(seconds):
    motor = Motor('C')
    motor.set_default_speed(-100)
    motor.run_for_seconds(seconds)

def lower_rack_and_pinion_for(seconds):
    motor = Motor('C')
    motor.set_default_speed(100)
    motor.run_for_seconds(seconds)

# wait for arrow, reduce time delay before run
def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('2') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)
    myhub.status_light.on('blue')

def M4():
    # solar farm
    motor = hub.port.D.motor
    stop_at_line(0,-30)
    turn_to(90)
    degrees_in_centimeter = 1/(5.6*3.14159) * 360
    move_forward(300 - 2*degrees_in_centimeter,90,50)
    turn_to(0)
    move_forward(350,0,50)
    turn_to(55)
    #motor.run_for_degrees(360 * 3, 100) # lower innovation project model, prepare to drop
    move_forward(100,55,50)
    move_forward(350 + 2*degrees_in_centimeter,90,50)
    turn_to(160)
    move_forward(800,160,50)
    motorE = Motor('E')
    motorE.run_for_rotations(0.5,-50)
    motorE.run_for_rotations(0.7,50)
    #wait_any_arrow_press()
    drop = 0.60 # keep innovation project down and up in "sync"
    motorD.run_for_rotations(drop, -30) # drop innovation project model
    motor.run_for_degrees(360 * drop, 50) # up
    move_forward(1000,129,80,stop=False)
    move_forward(1500,110,100)

def M3():
    move_forward(1000, 0, 50)
    move_forward(300, 30, 50)
    move_forward(100, 0, 50)
    stop_at_line(0, 30)
    move_forward(100,0,40)
    motorC.run_for_rotations(2.3, 50)
    wait_for_seconds(0.5)
    motor = hub.port.C.motor
    motor.run_for_degrees(828, -100)


###########################
# start calling functions #
###########################

wait_any_arrow_press()
robot.motion_sensor.reset_yaw_angle()

move_backward(-20,0)
robot.motion_sensor.reset_yaw_angle()
M3()
M4()
motorC.run_to_position(10)
print('Total time: {} seconds'.format(timer.now()))
