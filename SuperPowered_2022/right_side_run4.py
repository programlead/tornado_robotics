# LEGO type:standard slot:4 autostart

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *
from spike.operator import greater_than_or_equal_to

hub = PrimeHub()

a = ColorSensor("A")
b = ColorSensor("B")
c = Motor("C")
d = Motor("D")
e = Motor("E")
f = Motor("F")
right = Motor('F')
m = MotorPair("E","F")
t = Timer()

# wait for arrow, reduce time delay before run
def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('4') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)
    myhub.status_light.on('blue')

def get_current_power(acceleration_dist, power): # for acceleration
    rotations = acceleration_dist/(5.6 * 3.14159)
    degrees = rotations * 360
    current_power = (power - 30)/degrees * right.get_degrees_counted() + 30
    if current_power > power:
        current_power = power
    return current_power

def gyro_straight(degree, angle, kp, power): # gyro straight
    right.set_degrees_counted(0)
    while (fabs(right.get_degrees_counted()) < degree):
        correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        current_power = round(get_current_power(10, power))
        m.start_tank_at_power(current_power - correction, current_power + correction)
    m.stop()

def gyro_straight_dist(centimeters, angle, kp, power):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power)

wait_any_arrow_press()

m.set_stop_action("brake")
m.set_motor_rotation(17.6, "cm")
hub.motion_sensor.reset_yaw_angle()
m.set_default_speed(35)#35 is best speed

################
# start moving #
################

# television
gyro_straight_dist(30, 0, 1, -35)

# toy factory
m.move(2,"cm")
m.set_default_speed(60)
m.start_tank(20,0)
while(hub.motion_sensor.get_yaw_angle()<90):pass
m.move(7,"in")
m.start_tank(20,0)
while(hub.motion_sensor.get_yaw_angle()<140):pass
m.move(7.3,"in")
m.start_tank(0,20)
while(hub.motion_sensor.get_yaw_angle()>40):pass
m.set_default_speed(40)
m.move(15,"cm")

# hybrid car
m.move(-7,"in")
m.start_tank(20,0)
while(hub.motion_sensor.get_yaw_angle()<127):pass
#m.set_default_speed(65)
m.move(8,"in") # go to the hybrid car
m.stop()
d.run_for_rotations(0.4,30)
m.start_tank(20,0)
while(hub.motion_sensor.get_yaw_angle()<150):pass
m.stop()
d.run_for_rotations(-0.25,100)
m.start_tank(0,20)
while(hub.motion_sensor.get_yaw_angle()>140):pass
m.move(-50,"in",0,100)
m.stop()
print(t.now(),"seconds")
raise SystemExit
