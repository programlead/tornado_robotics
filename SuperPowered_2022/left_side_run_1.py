# LEGO type:standard slot:1 autostart

'''
10/30/2022 initial commit
10/31/2022 to smart grid
11/05/2022 full run
'''

from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from spike.operator import greater_than, less_than, equal_to
from math import *
import hub as hub_module
import sys

# initialization
hub = PrimeHub()
left_eye = ColorSensor('A')
right_eye = ColorSensor('B')
left = Motor('E')
right = Motor('F')
arm_left = Motor('C')
arm_left_nonblock = hub_module.port.C.motor
arm_right = Motor('D')
arm_right_nonblock = hub_module.port.D.motor
m = MotorPair('E', 'F')

# define function(s)
class GlobalYaw:
    def __init__(self):
        self.last_yaw = hub.motion_sensor.get_yaw_angle()
        self.rotations = 0

    def get_global_yaw(self):
        current_yaw = hub.motion_sensor.get_yaw_angle()
        if self.last_yaw - current_yaw >= 180:
            self.rotations += 1
        elif self.last_yaw - current_yaw <= -180:
            self.rotations -= 1
        global_yaw = current_yaw + self.rotations * 360
        self.last_yaw = current_yaw
        return global_yaw

def turn_to(target, power): # gyro turn
    yaw = global_yaw.get_global_yaw()
    if yaw < target:
        m.start_tank_at_power(power, -power)
        wait_until(global_yaw.get_global_yaw, greater_than, target)
        m.stop()
    else:
        m.start_tank_at_power(-power, power)
        wait_until(global_yaw.get_global_yaw, less_than, target)
        m.stop()

def gyro_straight(degree, angle, kp, power): # gyro straight
    right.set_degrees_counted(0)
    while (right.get_degrees_counted() < degree):
        #correction = (hub.motion_sensor.get_yaw_angle() - angle) * kp
        correction = (global_yaw.get_global_yaw() - angle) * kp
        m.start_tank_at_power(power - correction, power + correction)
    m.stop()

def gyro_straight_dist(centimeters, angle, kp, power):
    rotations = centimeters/(5.6 * 3.14159)
    gyro_straight(rotations * 360, angle, kp, power)

def detect_line(white, black, left_eye, power, steering=0):
    if left_eye:
        eye = ColorSensor('A')
    else:
        eye = ColorSensor('B')
    m.start(steering, power)
    wait_until(eye.get_reflected_light, greater_than, white)
    wait_until(eye.get_reflected_light, less_than, black)
    wait_until(eye.get_reflected_light, greater_than, white)

class LineFollower:
    def __init__(self, t_rli, power, kp, ki=0, kd=0):
        self.I = 0
        self.last_err = 0

        self.t_rli = t_rli
        self.power = power
        self.kp = kp
        self.ki = ki
        self.kd = kd

    def follow_line_one_step(self):
        err = left_eye.get_reflected_light() - self.t_rli
        D = err - self.last_err
        cor = (err * self.kp + self.I * self.ki + D * self.kd) * 1.5
        self.I = self.I * 0.99 + err
        self.last_err = err
        m.start_tank_at_power(round(self.power - cor), round(self.power + cor))

    def follow_line_forever(self):
        while True:
            self.follow_line_one_step()

    def follow_line_distance(self, centimeters):
        right.set_degrees_counted(0)
        rotations = centimeters/(5.6 * 3.14159)
        degrees = rotations * 360
        while (right.get_degrees_counted() < degrees):
            self.follow_line_one_step()


# wait for arrow, reduce time delay before run
def wait_any_arrow_press():
    # clear the status first
    myhub = PrimeHub()
    myhub.right_button.is_pressed()
    myhub.left_button.is_pressed()
    myhub.light_matrix.write('1') # write the slot
    myhub.status_light.on('green')
    while not myhub.right_button.is_pressed() and not myhub.left_button.is_pressed(): pass
    myhub.speaker.beep(80)
    myhub.status_light.on('blue')

# more initialization
wait_any_arrow_press()
start_angle = 0
run_speed = 70
turn_speed = 30
line_follower = LineFollower(50, 40, -0.2, -0.001, -2)
hub.motion_sensor.reset_yaw_angle()
global_yaw = GlobalYaw()
left.set_degrees_counted(0)
right.set_degrees_counted(0)
t = Timer()

'''# get to missions
gyro_straight_dist(71.5, 0 - start_angle, 1, 80)
#sys.exit(0)
turn_to(42, 40)'''

# Misson 11 Hydroelectric Dam
m.start_at_power(-40)
wait_for_seconds(0.2)
m.stop()
arm_left_nonblock.run_for_degrees(round(-1.8 * 360), -40) # lift rack and pinion
gyro_straight_dist(3, 0 - start_angle, 1, 40)
turn_to(67 - start_angle, turn_speed)
gyro_straight_dist(25, 67 - start_angle, 1, run_speed)
arm_right.run_for_degrees(100, 100) # hit the looped water unit
arm_right.run_for_degrees(-160, 100) # swipe back
gyro_straight_dist(10 , 63 - start_angle, 1, run_speed)
right.run_for_degrees(100, turn_speed)
m.move(-5, 'cm', 0, 30) # release the water

# Hydroelectric Dam -> Power Plant
m.move(3, 'cm', 0, 40)
turn_to(77 - start_angle, turn_speed)
gyro_straight_dist(40 , 77 - start_angle, 1, run_speed)
detect_line(90, 25, False, 30) ; m.stop()
turn_to(185 - start_angle, 25)

# Mission 10 Power Plant
#line_follower.follow_line_distance(13)
detect_line(90, 25, False, 25) ; m.stop()
arm_left.run_for_degrees(round(1 * 360), 100)
m.move_tank(15,'cm',-30,-30)

# Power Plant -> Smart Grid & Smart Grid
arm_left_nonblock.run_for_degrees(round(-1 * 360), -100)
turn_to(2 - start_angle, turn_speed)
arm_right_nonblock.run_for_degrees(220, 8)
gyro_straight_dist(50, 4 - start_angle, 1, 40)

# Smart Grid -> Oil Platform & Oil Platform
m.move(-2, 'cm', 0, 40)
turn_to(-88, turn_speed)
arm_left_nonblock.run_for_degrees(round(-1.8 * 360), 60)
line_follower.follow_line_distance(52)
detect_line(90, 25, False, 25, steering=-2)
m.move(4.5, 'cm', steering=0, speed=30)
for i in range(3):
    arm_left.run_for_degrees(round(-1.2 * 360), 100) # lift
    arm_left.run_for_degrees(round(1.2 * 360), 100) # lower
m.move(-4, 'cm', 0, 30)

# go back home
turn_to(-180, 30)
#gyro_straight_dist(30, -180 - start_angle, 1, 60)
#gyro_straight_dist(10, -160 - start_angle, 1, 60)
#gyro_straight_dist(30, -145 - start_angle, 1, 60)
m.move(80, 'cm', steering=10, speed=80)
#m.start(steering=10, speed=60)
m.stop()

# prepare to accept run 2 attachment
arm_left.run_to_position(45, direction='counterclockwise')
arm_right.run_to_position(45, direction='counterclockwise')

# total time
print('Total Time = {} seconds'.format(t.now()))
print('FINISH!!! :3 :D :)\n\n')
raise SystemExit
