from hub import button, light, light_matrix, motion_sensor, port, sound
import motor_pair, runloop

async def move_for_distance(pair, distance, steering, velocity=360):
    # Move the robot for the specified distance (centimeters)
    radius = 2.8
    circum = 6.28 * radius
    await motor_pair.move_for_degrees(pair, int((distance/circum) * 360), steering, velocity=velocity)

async def main():
    motor_pair.pair(motor_pair.PAIR_1, port.E, port.F)
    await move_for_distance(motor_pair.PAIR_1, 40, -1, velocity=2880)
    await move_for_distance(motor_pair.PAIR_1, 40, 0, velocity=-2880)

runloop.run(main())
