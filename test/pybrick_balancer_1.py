from pybricks.hubs import PrimeHub
from pybricks.pupdevices import Motor, ColorSensor, UltrasonicSensor, ForceSensor
from pybricks.parameters import Button, Color, Direction, Port, Side, Stop
from pybricks.robotics import DriveBase
from pybricks.tools import wait, StopWatch

hub = PrimeHub()

LW = Motor(Port.E, Direction.COUNTERCLOCKWISE)
RW = Motor(Port.F)
robot = DriveBase(LW, RW, wheel_diameter=56, axle_track=112)

class Segway:
    def __init__(self, left, right):
        self.lw = left
        self.rw = right

    def dc(self, duty):
        self.lw.dc(duty)
        self.rw.dc(duty)

    def pid(self, dc, kp, ki, kd):
        target_pitch = 24
        dead_power = 17
        timer = StopWatch()
        t0 = timer.time()
        wait(1)

        oldErr = 0
        self.lw.reset_angle()
        I = 0
        while True:
            travel = self.lw.angle()

            adjusted_target_pitch = target_pitch + travel / 360
           
            pitch = hub.imu.tilt()
            err = pitch[0] - adjusted_target_pitch
            
            t = timer.time()
            dT = t - t0
            t0 = t

            I = I * 0.995 +  err * dT 
            correction = err * kp + (err - oldErr)  * kd / dT + I * ki
            print(I)
            if correction > 1:
                correction += dead_power
            if correction < -1:
                correction -= dead_power

            segway.dc(correction)
            wait(10)

segway = Segway(LW, RW)

segway.pid(30, 5, 1e-2, 3)
segway.dc(17)
wait(200)

