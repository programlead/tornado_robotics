from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *

print('============================RESTART============================')

# initialization
hub = PrimeHub()
a = ColorSensor('A')
b = ColorSensor('B')
c = Motor('C')
d = Motor('D')
e = Motor('E')
f = Motor('F')
m = MotorPair('E', 'F')
t = Timer()

# define the line follower
def follow_line(t_rli, power, kp, ki, kd):
    I = 0
    D = 0
    last_err = 0
    while True:
        err = a.get_reflected_light() - t_rli
        D = err - last_err
        cor = (err * kp + I * ki + D * kd) * 1.5
        I = I * 0.99 + err
        last_err = err
        m.start_tank_at_power(round(power - cor), round(power + cor))

# make the line follower a class
class LineFollower:
    def __init__(self, t_rli, power, kp, ki, kd):
        self.I = 0
        self.last_err = 0

        self.t_rli = t_rli
        self.power = power
        self.kp = kp
        self.ki = ki
        self.kd = kd

    def follow_line_one_step(self):
        err = a.get_reflected_light() - self.t_rli
        D = err - self.last_err
        cor = (err * self.kp + self.I * self.ki + D * self.kd) * 1.5
        self.I = self.I * 0.99 + err
        self.last_err = err
        m.start_tank_at_power(round(self.power - cor), round(self.power + cor))
    
    def follow_line_forever(self):
        while True:
            self.follow_line_one_step()

# start the program
line_follow = LineFollower(75, 40, 0.3, 0.003, 2)
#line_follow.follow_line_forever()
follow_line(75, 40, 0.3, 0.003, 2)
